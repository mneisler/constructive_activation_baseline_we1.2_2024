# constructive_activation_baseline_WE1.2_2024

## Description

Analysis to establish baseline of constructive activation to help inform the key result identifed for WE1.2 for the FY24-25 Wikimedia Foundation Annual Plan.

Constructive activation is defined as "A newcomer making their first edit to an article in the main namespace on a mobile device within 24 hours of registration, and that edit not being reverted within 48 hours of being published."