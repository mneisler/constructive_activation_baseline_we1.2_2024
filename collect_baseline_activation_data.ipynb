{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "8258a1c5-a5a2-400e-b50d-e48d0c390529",
   "metadata": {},
   "source": [
    "# Collect baseline activation edit data\n",
    "\n",
    "This notebook is based on notebooks written for Growth's Structured Task Analyses by [Morten Warncke-Wang (WMF)](https://meta.wikimedia.org/wiki/User:MWang_(WMF)), as documented in this [repo](https://gitlab.wikimedia.org/nettrom/2021-Growth-structured-tasks).\n",
    "\n",
    "Per constructive activation definition, collect all first edits by a newcomer to an article in a main namespace on a mobile device made within 24 hours of registration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "faa1b91e-9dcb-4935-95ed-a1c15623fdee",
   "metadata": {},
   "outputs": [],
   "source": [
    "# load required packing\n",
    "import json\n",
    "import datetime as dt\n",
    "\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "\n",
    "from wmfdata import spark, mariadb"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "69a083c2-63ad-4d0c-ab8b-9f956b978042",
   "metadata": {},
   "outputs": [],
   "source": [
    "# use the legacy version of spark to correctly parse dates\n",
    "spark.run(\"set spark.sql.legacy.timeParserPolicy=LEGACY\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "252841c4-316e-4264-9aa4-12f31b4745dc",
   "metadata": {},
   "outputs": [],
   "source": [
    "## Configuration variables\n",
    "\n",
    "## Timestamps of the user registrations gathered in \"collect_user_data.ipynb\"\n",
    "start_ts = dt.datetime(2024, 3, 1, 0, 0, 0)\n",
    "end_ts = dt.datetime(2024, 6, 1, 0, 0, 0)\n",
    "\n",
    "\n",
    "## The snapshot of mediawiki_history that we'll use\n",
    "mwh_snapshot = '2024-06'\n",
    "\n",
    "## The name of the table with the user dataset (from the \"collect_user_data.ipynb\" notebook)\n",
    "canonical_user_table = 'mneisler.contructive_activation_users_2024'\n",
    "\n",
    "## Output file name\n",
    "edit_data_output_filename = 'data/activation-edit-data.tsv'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5f4b10e0-e5f9-42b2-9f7a-8ebc1f2fd00f",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Collecting Edit Data\n",
    "\n",
    "Gather editing data needed to calculate constructive activation. This will include both newcomer edit and revert counts on their first day.\n",
    "\n",
    "We limited the edits to the following:\n",
    "* Completed on a main namespace (page namespace = 0)\n",
    "* Completed on a WIkipedia Project\n",
    "* Completed on a mobile app or mobile web device\n",
    "* Completed within 24 hours of the editors registration time. \n",
    "\n",
    "In the query, I gathered edits completed for mobile web and each app type, and all mobile apps together. This was done to confirm that app edits can be considered exclusive and can be summed together if needed to identify constructive activations across all mobile devices."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6c15a4ee-4cca-4503-a590-039b014f6475",
   "metadata": {},
   "outputs": [],
   "source": [
    "edit_data_query = '''\n",
    "WITH edits AS (\n",
    "    SELECT \n",
    "        wiki_db, \n",
    "        event_user_id AS user_id,\n",
    "     -- all ns 0 ios edits on the first day\n",
    "    SUM(IF(array_contains(revision_tags, 'ios app edit') \n",
    "        AND unix_timestamp(event_timestamp) - unix_timestamp(event_user_creation_timestamp) < 86400, 1, 0))\n",
    "        AS num_ns0_edits_24hrs_ios,\n",
    "  -- all ns 0 ios edits on the first day that were reverted\n",
    "    SUM(IF(array_contains(revision_tags, 'ios app edit') \n",
    "        AND revision_is_identity_reverted = true AND revision_seconds_to_identity_revert < 60*60*48\n",
    "        AND unix_timestamp(event_timestamp) - unix_timestamp(event_user_creation_timestamp) < 86400, 1, 0))\n",
    "        AS num_ns0_reverts_24hrs_ios,\n",
    "  -- all ns 0 android edits on the first day\n",
    "     SUM(IF(array_contains(revision_tags, 'android app edit') \n",
    "        AND unix_timestamp(event_timestamp) - unix_timestamp(event_user_creation_timestamp) < 86400, 1, 0))\n",
    "        AS num_ns0_edits_24hrs_android,\n",
    " -- all ns 0 android edits on the first day that were reverted\n",
    "    SUM(IF(array_contains(revision_tags, 'android app edit') \n",
    "        AND revision_is_identity_reverted = true AND revision_seconds_to_identity_revert < 60*60*48\n",
    "        AND unix_timestamp(event_timestamp) - unix_timestamp(event_user_creation_timestamp) < 86400, 1, 0))\n",
    "        AS num_ns0_reverts_24hrs_android,\n",
    "  -- all ns 0 mobile web edits on the first day\n",
    "     SUM(IF(array_contains(revision_tags, 'mobile web edit') \n",
    "        AND unix_timestamp(event_timestamp) - unix_timestamp(event_user_creation_timestamp) < 86400, 1, 0))\n",
    "        AS num_ns0_edits_24hrs_mobileweb,\n",
    " -- all ns 0 mobile web edits on the first day that were reverted\n",
    "    SUM(IF(array_contains(revision_tags, 'mobile web edit') \n",
    "        AND revision_is_identity_reverted = true AND revision_seconds_to_identity_revert < 60*60*48\n",
    "        AND unix_timestamp(event_timestamp) - unix_timestamp(event_user_creation_timestamp) < 86400, 1, 0))\n",
    "        AS num_ns0_reverts_24hrs_mobileweb,\n",
    " -- all ns 0 mobile app edits on the first day just a check to confirm this matches sum of ios and android\n",
    "     SUM(IF(array_contains(revision_tags, 'mobile app edit') \n",
    "        AND unix_timestamp(event_timestamp) - unix_timestamp(event_user_creation_timestamp) < 86400, 1, 0))\n",
    "        AS num_ns0_edits_24hrs_mobileapp,\n",
    " -- all ns 0 mobile web edits on the first day that were reverted\n",
    "    SUM(IF(array_contains(revision_tags, 'mobile app edit') \n",
    "        AND revision_is_identity_reverted = true AND revision_seconds_to_identity_revert < 60*60*48\n",
    "        AND unix_timestamp(event_timestamp) - unix_timestamp(event_user_creation_timestamp) < 86400, 1, 0))\n",
    "        AS num_ns0_reverts_24hrs_mobileapp\n",
    "    FROM wmf.mediawiki_history\n",
    "    -- limit to only wikipedias\n",
    "    INNER JOIN canonical_data.wikis\n",
    "    ON\n",
    "        wiki_db = database_code and\n",
    "        database_group ==  'wikipedia'\n",
    "    WHERE snapshot = \"{snapshot}\"\n",
    "    AND event_entity = \"revision\"\n",
    "    AND event_type = \"create\"\n",
    "    -- limit to only main namespaces\n",
    "    AND page_namespace = 0\n",
    "    -- limit to only mobile devices\n",
    "     AND (array_contains(revision_tags, 'ios app edit') OR\n",
    "          array_contains(revision_tags, 'android app edit') OR\n",
    "          array_contains(revision_tags, 'mobile web edit'))\n",
    "    AND event_timestamp > \"{start_date}\"\n",
    "    AND event_timestamp < \"{end_date}\"\n",
    "    GROUP BY \n",
    "        wiki_db, \n",
    "        event_user_id\n",
    "),\n",
    "users AS (\n",
    "    SELECT\n",
    "        wiki_db,\n",
    "        user_id,\n",
    "        user_registration_timestamp,\n",
    "        reg_on_mobile,\n",
    "        reg_w_api\n",
    "    FROM {exp_user_table}\n",
    ")\n",
    "SELECT\n",
    "    users.wiki_db,\n",
    "    users.user_id,\n",
    "    users.user_registration_timestamp,\n",
    "    users.reg_on_mobile,\n",
    "    users.reg_w_api,\n",
    "    COALESCE(num_ns0_edits_24hrs_ios, 0) AS num_article_edits_24hrs_ios,\n",
    "    COALESCE(num_ns0_reverts_24hrs_ios, 0) AS num_article_reverts_24hrs_ios,\n",
    "    COALESCE(num_ns0_edits_24hrs_android, 0) AS num_article_edits_24hrs_android,\n",
    "    COALESCE(num_ns0_reverts_24hrs_android, 0) AS num_article_reverts_24hrs_android,\n",
    "    COALESCE(num_ns0_edits_24hrs_mobileweb, 0) AS num_article_edits_24hrs_mobileweb,\n",
    "    COALESCE(num_ns0_reverts_24hrs_mobileweb, 0) AS num_article_reverts_24hrs_mobileweb,\n",
    "    COALESCE(num_ns0_edits_24hrs_mobileapp, 0) AS num_article_edits_24hrs_mobileapp,\n",
    "    COALESCE(num_ns0_reverts_24hrs_mobileapp, 0) AS num_article_reverts_24hrs_mobileapp\n",
    "FROM users\n",
    "LEFT JOIN edits\n",
    "ON users.wiki_db = edits.wiki_db\n",
    "AND users.user_id = edits.user_id\n",
    "'''"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4d06e7d8-7b4d-4174-91a1-e3a88644ca60",
   "metadata": {},
   "outputs": [],
   "source": [
    "## We give users who registered within 2 days of the last\n",
    "## date the same amount of time to edit as everyone else.\n",
    "\n",
    "all_users_edit_data = spark.run(\n",
    "    edit_data_query.format(\n",
    "        snapshot = mwh_snapshot,\n",
    "        start_date = start_ts.date().isoformat(),\n",
    "        end_date = (end_ts.date() + dt.timedelta(days = 2)).isoformat(),\n",
    "        exp_user_table = canonical_user_table\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2bf3dc71-785f-4707-accc-a0ebbaa4e74d",
   "metadata": {},
   "outputs": [],
   "source": [
    "len(all_users_edit_data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0384350f-98b4-4e2a-9577-f63f219879bb",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_users_edit_data.loc[all_users_edit_data['num_article_edits_24hrs_mobileweb'] > 0].head()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2e8a6c6c-4ddb-4e98-9c69-a5109e87e8d0",
   "metadata": {},
   "source": [
    "Write out the canonical edit dataset for importing into R."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5eee52be-b056-4ca7-a97d-61ea6b612177",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_users_edit_data.to_csv(edit_data_output_filename,\n",
    "                           header = True, index = False, sep = '\\t')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6e9794e6-8598-43ab-85d0-9be51bfa566f",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
