{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "fda8869e-c282-46d9-937f-ef59d2ae6354",
   "metadata": {},
   "source": [
    "# WE 1.2: Establish baseline for constructive activation\n",
    "\n",
    "**Megan Neisler, Staff Data Scientist, Wikimedia Foundation**\n",
    "\n",
    "**23 July 2024**\n",
    "\n",
    "[Task](https://phabricator.wikimedia.org/T360829)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a84c09ee-1284-4f99-acf4-c8fb357c497a",
   "metadata": {},
   "source": [
    "## Purpose\n",
    "\n",
    "The purpose of this analysis is to establish a baseline of constructive activation, as defined by the WE 1.2 KR, to help inform key resuts for WE1.2 of the FY24-25 Wikimedia Foundation Annual Plan.\n",
    "\n",
    "We gathered registrations in March, April, and May 2024, the three most recent months available prior to the end of the fiscal year. This will be used to compare to status at the end of the upcoming fiscal year. For those registrations, we gather data on activation by gathering data on edits to a main namespace completed on a mobile device and the reverts of those edits.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "67faf90c-791d-4141-a7e7-4eab01c215b2",
   "metadata": {},
   "source": [
    "## Definition\n",
    "\n",
    "We defined constructive activation as: \"A newcomer making their first edit to an article in the **main namespace on a mobile device** within 24 hours of registration, and that edit not being reverted within 48 hours of being published.\"\n",
    "\n",
    "Current KR language: \"Constructive Activation: Widespread deployment of interventions shown to cause #% increase in the percentage of newcomers who publish ≥1 constructive edit in the main namespace on a mobile device, as measured by controlled experiments.\" Note this can be revised and iterated as needed. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05e42ed3-fe32-46bd-95d6-26ce9c7b894d",
   "metadata": {},
   "source": [
    "## Methodology\n",
    "\n",
    "We gathered mobile registrations from March, April, and May 2024, as the most recent months available prior to the end of this fiscal year.  We also collect data on API registrations as those are typically mobile apps.  For those registrations, we gather data on edits to a main namespace completed on a mobile device within 24 hours of registration and the reverts of those edits.\n",
    "\n",
    "A newcomers is identified as being constructively activated if they complete at least 1 edit within 24 hours of registration on a wiki, and that edit not being reverted within 48 hours. In other words, if a newcomer completes 3 edits within 24 hours of registering and only 2 of those are reverted, they would still meet the criteria for constructive activation.\n",
    "\n",
    "We reviewed splits by platform (mobile web, iOS, and Android) and by registration month to identify trends over time. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b1f3a398-9f40-48a5-8826-9204ecb23753",
   "metadata": {},
   "outputs": [],
   "source": [
    "shhh <- function(expr) suppressPackageStartupMessages(suppressWarnings(suppressMessages(expr)))\n",
    "shhh({\n",
    "    library(lubridate)\n",
    "    library(ggplot2)\n",
    "    library(dplyr)\n",
    "})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6e7ba58c-3dc8-4217-9e26-f8abd661c124",
   "metadata": {},
   "outputs": [],
   "source": [
    "options(dplyr.summarise.inform = FALSE)\n",
    "options(repr.plot.width = 15, repr.plot.height = 10)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a7b5f6f5-c61f-42b3-8ead-747645047c83",
   "metadata": {},
   "source": [
    "# Data Gathering and Cleaning"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "85b420b0-b083-4226-bf52-f3de9ce5e116",
   "metadata": {},
   "outputs": [],
   "source": [
    "# load data for assessing activations\n",
    "all_users_edit_data <-\n",
    "  read.csv(\n",
    "    file = 'data/activation-edit-data.tsv',\n",
    "    header = TRUE,\n",
    "    sep = \"\\t\",\n",
    "    stringsAsFactors = FALSE\n",
    "  ) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7cc3861e-2d5a-4894-877a-10a11fe03a75",
   "metadata": {},
   "outputs": [],
   "source": [
    "#reformat user-id and adjust to include wiki to account for duplicate user id instances.\n",
    "# Users do not have the smae user_id on different wikis\n",
    "all_users_edit_data$user_id <-\n",
    "  as.character(paste(all_users_edit_data$user_id,all_users_edit_data$wiki_db,sep =\"-\" ))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c47230d0-ffc5-4099-b3ab-badf1f9c4ab0",
   "metadata": {},
   "source": [
    "## Some exploration:\n",
    "\n",
    "How many newcomers make at least one edit on their first day split by number of edits?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "33b1a3b0-74b4-40e2-aa8e-8d35b3513850",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Edits by platform\n",
    "all_users_edit_data %>%\n",
    "summarise(num_article_edits_24hrs_ios = sum(num_article_edits_24hrs_ios),\n",
    "          num_article_edits_24hrs_android = sum(num_article_edits_24hrs_android),\n",
    "          num_article_edits_24hrs_mobileweb = sum(num_article_edits_24hrs_mobileweb),\n",
    "         num_article_edits_24hrs_mobileapp = sum(num_article_edits_24hrs_mobileapp))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e04cbae5-843c-400e-83ff-655ce625ec43",
   "metadata": {},
   "outputs": [],
   "source": [
    "Confirmed that the mobile app revision tag equates the sum of iOS and Android apps."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f5d0002e-91a7-487e-9939-59534f816d92",
   "metadata": {},
   "outputs": [],
   "source": [
    "#mobile web editors\n",
    "all_users_edit_data %>%\n",
    "filter(reg_on_mobile == 1) %>% \n",
    "filter(num_article_edits_24hrs_mobileweb > 0) %>% #limit to editors\n",
    "mutate(edit_count_group = ifelse(num_article_edits_24hrs_mobileweb > 1, \"more than one\", \"less than one\")) %>%\n",
    "group_by(edit_count_group) %>%\n",
    "summarise(n_editors = n_distinct(user_id))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a92e409f-bca0-430e-adb9-1fbcdeffdd47",
   "metadata": {},
   "source": [
    "\n",
    "Almost half (about 46%) of mobile editors make more than one mobile edit on their first day. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3f800cb2-87a9-4580-85a7-e7962427fd30",
   "metadata": {},
   "outputs": [],
   "source": [
    "# android users\n",
    "all_users_edit_data %>%\n",
    "filter(reg_w_api == 1) %>%  #app registrations\n",
    "filter(num_article_edits_24hrs_android > 0) %>% #limit to editors\n",
    "mutate(edit_count_group = ifelse(num_article_edits_24hrs_android > 1, \"more than one\", \"less than one\")) %>%\n",
    "group_by(edit_count_group) %>%\n",
    "summarise(n_editors = n_distinct(user_id))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "64516e7f-a026-40a6-9e49-d31f568c420d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# iOS users\n",
    "all_users_edit_data %>%\n",
    "filter(reg_w_api == 1) %>%  #app registrations\n",
    "filter(num_article_edits_24hrs_ios > 0) %>% #limit to editors\n",
    "mutate(edit_count_group = ifelse(num_article_edits_24hrs_ios > 1, \"more than one\", \"less than one\")) %>%\n",
    "group_by(edit_count_group) %>%\n",
    "summarise(n_editors = n_distinct(user_id))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6ecf67b3-40f5-4c25-b610-9a85a9c50a7b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# All mobile app users\n",
    "all_users_edit_data %>%\n",
    "filter(reg_w_api == 1) %>%  #app registrations\n",
    "filter(num_article_edits_24hrs_mobileapp > 0) %>% #limit to editors\n",
    "mutate(edit_count_group = ifelse(num_article_edits_24hrs_mobileapp > 1, \"more than one\", \"less than one\")) %>%\n",
    "group_by(edit_count_group) %>%\n",
    "summarise(n_editors = n_distinct(user_id))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9a714a0a-fecf-46b6-a1e6-e745912b08d3",
   "metadata": {},
   "source": [
    "A little over half (52%) of mobile app newcomers make more than one edit their first day.  \n",
    "\n",
    "Mobile app editors represent about only 3% of all newcomers that made an edit within 24 hours of registering on a mobile device. 4% of all mobile edits by newcomers.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a0f2ecff-78be-4184-b785-707443324ed4",
   "metadata": {},
   "source": [
    "# Define activation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "95370258-299c-4377-ac12-e3dfc3980f99",
   "metadata": {},
   "source": [
    "## Definition\n",
    "A newcomer making an edit to an article in the **main namespace on a mobile device** within 24 hours of registration, and that edit not being reverted within 48 hours of being published.\"\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7ae3ff20-fb43-41e6-a3aa-2c26b6b26f1b",
   "metadata": {},
   "outputs": [],
   "source": [
    "## add columns to define overall activation\n",
    "all_users_edit_data <- all_users_edit_data %>%\n",
    "    mutate(is_activated = ifelse(\n",
    "        num_article_edits_24hrs_mobileweb | num_article_edits_24hrs_ios | num_article_edits_24hrs_android\n",
    "        > 0, 'is_activated', 'is_not_activated'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a1586ff6-fdfe-4c27-840a-8d40b9f1cdde",
   "metadata": {},
   "outputs": [],
   "source": [
    "## add column to define overall constructive activation\n",
    "all_users_edit_data <- all_users_edit_data %>%\n",
    "    mutate(is_constr_activated = ifelse(\n",
    "        (num_article_edits_24hrs_mobileweb - num_article_reverts_24hrs_mobileweb) > 0 |\n",
    "        (num_article_edits_24hrs_ios - num_article_reverts_24hrs_ios) > 0 |\n",
    "        (num_article_edits_24hrs_android - num_article_reverts_24hrs_android) > 0,\n",
    "        'is_constr_activated', 'not_constr_activated'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "631d7169-7f19-46ec-97f0-3874527a86d2",
   "metadata": {},
   "outputs": [],
   "source": [
    "## add column to define constructive activation by platform\n",
    "## combining apps together as there's not a great way to decipher iOS and Andoid regsitrations separately\n",
    "all_users_edit_data <- all_users_edit_data %>%\n",
    "    mutate(is_constr_activated_platform = \n",
    "           case_when(\n",
    "        (num_article_edits_24hrs_mobileweb - num_article_reverts_24hrs_mobileweb) > 0 ~ \"constr_activation_mobileweb\",\n",
    "        ((num_article_edits_24hrs_mobileapp - num_article_reverts_24hrs_mobileapp > 0)) ~ \"constr_activation_apps\",\n",
    "        TRUE ~ 'not_constr_activated'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "076467a8-9c8d-4f70-97f6-24ca71f5fab3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# check that activation was defined appropriately\n",
    "all_users_edit_data %>%\n",
    "    filter(is_activated == 'is_activated',\n",
    "         num_article_edits_24hrs_ios > 0 ) %>%\n",
    "    slice_head(n= 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d411f29-1bad-4925-8483-783cde402b58",
   "metadata": {},
   "source": [
    "# Define Registration Month"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "92680401-685a-4e32-a0c9-583eafc0c04e",
   "metadata": {},
   "source": [
    "We also want to split by month because we're curious about trends across time.\n",
    "\n",
    "Since the registration timestamp is an ISO 8601 timestamp, we grab the first 7 characters of it (\"YYYY-MM\")."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e3100c61-e4eb-4250-83a0-5735674dce6b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# format registration timestamp to day\n",
    "all_users_edit_data$user_registration_timestamp <- as.Date(all_users_edit_data$user_registration_timestamp, format = \"%Y-%m-%d\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17014893-add8-417d-9def-f678ee8b4c5f",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_users_edit_data <- all_users_edit_data %>%\n",
    "    mutate(month = floor_date(user_registration_timestamp, unit = 'month') )\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "36c85b68-3f55-40b9-a8e0-3e6e8c38a879",
   "metadata": {},
   "source": [
    "# Aggregate Data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4f0a3711-a3c6-4d08-a908-4f5e9de99cf8",
   "metadata": {},
   "source": [
    "## Overall on Mobile Web"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "085ec0e0-7cf9-4c16-a21d-1b93935f5892",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Mobile web\n",
    "constructive_activation_mobileweb <- all_users_edit_data %>%\n",
    "    filter(reg_on_mobile == 1\n",
    "          )  %>%    #limit to mobile registrations\n",
    "    group_by(is_constr_activated_platform) %>%\n",
    "    summarise(num_users = n()) %>%\n",
    "    mutate(pct_users = round(num_users/sum(num_users) *100, 2))  %>%\n",
    "   filter(is_constr_activated_platform == 'constr_activation_mobileweb') \n",
    "\n",
    "\n",
    "constructive_activation_mobileweb"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "321111e8-619a-47a0-a0ed-fb863229583b",
   "metadata": {},
   "source": [
    "## Overall on Apps"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "38fc39c3-1ee9-47e5-a7fb-d7a6246dbe62",
   "metadata": {},
   "outputs": [],
   "source": [
    "## Apps\n",
    "constructive_activation_platform_api <- all_users_edit_data %>%\n",
    "    filter(reg_w_api == 1,\n",
    "          is_constr_activated_platform != 'constr_activation_mobileweb')  %>%  # limit to app registrations\n",
    "    group_by(is_constr_activated_platform) %>%\n",
    "    summarise(num_users = n()) %>%\n",
    "    mutate(pct_users = round(num_users/sum(num_users) *100, 2))\n",
    "\n",
    "\n",
    "constructive_activation_platform_api"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ffa609b0-0418-447b-adb5-b5147a7f1dd3",
   "metadata": {},
   "source": [
    "## By Registration Month"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9b551bdd-c4fd-41d2-b5a1-6dc583b8fd77",
   "metadata": {},
   "outputs": [],
   "source": [
    "# mobile web by reg month\n",
    "constructive_activation_mobileweb_month <- all_users_edit_data %>%\n",
    "    filter(reg_on_mobile == 1\n",
    "          )  %>%    #limit to mobile registrations\n",
    "    group_by(month, is_constr_activated_platform) %>%\n",
    "    summarise(num_users = n_distinct(user_id)) %>%\n",
    "    mutate(pct_users = round(num_users/sum(num_users) *100, 2))  %>%\n",
    "   filter(is_constr_activated_platform == 'constr_activation_mobileweb') \n",
    "\n",
    "\n",
    "constructive_activation_mobileweb_month"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3cdc1310-fce4-4039-b35d-79bdf8babff6",
   "metadata": {},
   "outputs": [],
   "source": [
    "## Apps by reg month\n",
    "constructive_activation_platform_api <- all_users_edit_data %>%\n",
    "    filter(reg_w_api == 1,\n",
    "          is_constr_activated_platform != 'constr_activation_mobileweb')  %>%  # limit to app registrations\n",
    "    group_by(month, is_constr_activated_platform) %>%\n",
    "    summarise(num_users = n_distinct(user_id)) %>%\n",
    "    mutate(pct_users = round(num_users/sum(num_users) *100, 2)) %>%\n",
    "   filter(is_constr_activated_platform == 'constr_activation_apps') \n",
    "\n",
    "\n",
    "constructive_activation_platform_api"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68a81937-3ec6-42c2-b69a-df908678cd9e",
   "metadata": {},
   "source": [
    "## By Wiki"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a47d0165-cc6f-415f-81c6-a9029f298b72",
   "metadata": {},
   "outputs": [],
   "source": [
    "# mobile web by wiki\n",
    "constructive_activation_mobileweb_wiki <- all_users_edit_data %>%\n",
    "    filter(reg_on_mobile == 1\n",
    "          )  %>%    #limit to mobile registrations\n",
    "    group_by(wiki_db, is_constr_activated_platform) %>%\n",
    "    summarise(num_users = n_distinct(user_id)) %>%\n",
    "    mutate(pct_users = round(num_users/sum(num_users) *100, 2))  %>%\n",
    "   filter(is_constr_activated_platform == 'constr_activation_mobileweb')\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "80b3ccdb-b0dd-4e7e-828f-519a7eb02ef8",
   "metadata": {},
   "outputs": [],
   "source": [
    "## Find range of activation rates for wikis with over 100 users that made an edit\n",
    "constructive_activation_mobileweb_wiki %>%\n",
    "filter(num_users > 100) %>%\n",
    "arrange(pct_users)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6f321cec-e51a-47be-b663-b851bf99f68f",
   "metadata": {},
   "outputs": [],
   "source": [
    "## Apps by wiki\n",
    "constructive_activation_app_wiki <- all_users_edit_data %>%\n",
    "    filter(reg_w_api == 1,\n",
    "          is_constr_activated_platform != 'constr_activation_mobileweb')  %>%  # limit to app registrations\n",
    "    group_by( wiki_db, is_constr_activated_platform) %>%\n",
    "    summarise(num_users = n_distinct(user_id)) %>%\n",
    "    mutate(pct_users = round(num_users/sum(num_users) *100, 2)) %>%\n",
    "   filter(is_constr_activated_platform == 'constr_activation_apps')\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "199752fe-91d0-4b91-9ec5-08d7ab908ab9",
   "metadata": {},
   "outputs": [],
   "source": [
    "## Find range of activation rates for wikis with over 100 users that made an edit\n",
    "constructive_activation_app_wiki %>%\n",
    "filter(num_users >20) %>%\n",
    "arrange(pct_users)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c3fa34e8-25a5-41aa-89cf-1696a69e791c",
   "metadata": {},
   "source": [
    "# Reference \n",
    "\n",
    "This notebook is based on notebooks written for Growth's Structured Task Analyses by [Morten Warncke-Wang (WMF)](https://meta.wikimedia.org/wiki/User:MWang_(WMF)), as documented in this [repo](https://gitlab.wikimedia.org/nettrom/2021-Growth-structured-tasks).\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "4.3.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
